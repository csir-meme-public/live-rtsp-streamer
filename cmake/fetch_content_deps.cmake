include(FetchContent)

FetchContent_Declare(
    vpp
    GIT_REPOSITORY https://gitlab.com/csir-meme-public/vpp
    GIT_TAG        master
)

# CMake 3.14+
FetchContent_MakeAvailable(vpp)

