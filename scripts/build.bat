@echo off

if exist cmake_params (
set /p CMAKE_PARAMS=<cmake_params
)

if exist vcpkg_dependencies (
set /p VCPKG_PARAMS=<vcpkg_dependencies
echo "VCPKG dependencies: %VCPKG_PARAMS%"
)
sET TOOLCHAIN_PARAM=""

echo "Checking vcpkg triplet"
set VCPKG_TRIPLET_PARAM=--triplet x86-windows
if exist vcpkg_triplet_win (
set /p VCPKG_TRIPLET_WIN=<vcpkg_triplet_win
	echo "VCPKG VCPKG_TRIPLET_WIN: %VCPKG_TRIPLET_WIN%"
	SET /p VCPKG_TRIPLET_PARAM="--triplet %VCPKG_TRIPLET_WIN%"
)
echo "Checking vcpkg triplet done"

rem command line arguments
set /argc=0
setlocal enableextensions enabledelayedexpansion
call :getargc argc %*
echo Count is %argc%
echo Args are %*
goto start

:getargc
    set getargc_v0=%1
    set /a "%getargc_v0% = 0"
:getargc_l0
    if not x%2x==xx (
        shift
        set /a "%getargc_v0% = %getargc_v0% + 1"
        goto :getargc_l0
    )
    set getargc_v0=
    goto :eof

:start	

if exist vcpkg_dependencies (
iF NOT DEFINED VCPKG_DIR (
  echo "VCPKG_DIR env var not set - using submodule as default"
  SET VCPKG_DIR=%~dp0../libs/vcpkg  
)

if not exist %VCPKG_DIR% (
	echo "Warning: VCPKG_DIR %VCPKG_DIR% does not exist"
)

rem command line argument supercedes env var unless it does not exist
if %argc% EQU 1 ( 
	echo "Updating VCPKG_DIR dir to %1"
	rem Note the leading slash, if its not there, the statement causes the batch script to fail since the argument does not exist
    SET VCPKG_DIR=%1
) 

echo "Using VCPKG_DIR %VCPKG_DIR%"

@rem if the VCPKG dir has not been passed in via command line argument: we will use the local submodule for building
if not exist %VCPKG_DIR% (
	echo "VCPKG_DIR does not exist"
	goto eof
)

cd %VCPKG_DIR%
if not exist vcpkg.exe (
  if not exist bootstrap-vcpkg.bat (
    git submodule update --init
  )
  echo "Building vcpkg.exe"
  call bootstrap-vcpkg.bat
) else (
  echo "vcpkg.exe already exists"
)

echo ".\vcpkg.exe install %VCPKG_PARAMS%  %VCPKG_TRIPLET_PARAM% "

.\vcpkg.exe install %VCPKG_PARAMS%  %VCPKG_TRIPLET_PARAM% 

@rem https://superuser.com/questions/129969/navigate-to-previous-directory-in-windows-command-prompt
cd /D  %~dp0
@rem cd ..\..\scripts
sET TOOLCHAIN_PARAM="-DCMAKE_TOOLCHAIN_FILE=%VCPKG_DIR%/scripts/buildsystems/vcpkg.cmake"

)

if not exist ..build mkdir ..\build
  
cd ..\build
eCHO "Invoking CMake with TOOLCHAIN_PARAM=%TOOLCHAIN_PARAM%"

cmake %TOOLCHAIN_PARAM% %CMAKE_PARAMS% ^
  -A Win32 ..

if %ERRORLEVEL% EQU 0 (
  ECHO "Building Release version"
  cmake --build . --config Release 
::  ECHO "Building Debug version"
::  cmake --build . --config Debug 
) else (
  ECHO "Error generating build files: %ERRORLEVEL%"
  goto exit_error
)
  
cd /D  %~dp0  

goto exit_success


:exit_error
eCHO "Build failed"
eXIT /B 1

:exit_success

:eof
eCHO "Build succeeded"

endlocal

echo "END"
eXIT /B 0
